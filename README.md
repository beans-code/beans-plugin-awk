# AWK plugin for BEANS

`BEANS` is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables [https://gitlab.com/beans-code/beans](https://gitlab.com/beans-code/beans).

`AWK` is a domain-specific language designed for text processing and typically used as a data extraction and reporting tool [https://en.wikipedia.org/wiki/AWK](https://en.wikipedia.org/wiki/AWK).

## Features

`BEANS` supports `AWK` scripts and the main features are:

* in `AWK plugin` scripts one can use column names, not the number of columns. This makes the scripts much easier to read for others. 
* with `AWK plugin` you can easily read data from many different datasets and tables in parallel. Every tables is processed separatelly. 
* the output of the `AWK` script one can easily save to a table and use that table for further analysis

## Example

This example script is extracting two columns from a table in BEANS: `tphys` and `smt`. These two columns are filter and onlt the ones for which `tphys < 100.0` are kept: 

```awk
BEGIN {
	print "# tphys smt";
}

{
	if ($tphys < 100.0) print $tphys, $smt; 
}
```

The main advantage of using `AWK` scripts in `BEANS` is that you can use column names (not the column indices), you can read in parallel many different datasets and tables from within `BEANS` and you can save your results to `BEANS` for further analysis. 
