package net.beanscode.plugin.awk.unittests;

import junit.framework.TestCase;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.plugin.awk.AwkEntry;
import net.beanscode.plugin.awk.AwkUtils;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;

import org.junit.Test;


public class AwkUtilsTest extends TestCase {

//	@Test
//	public void testAwkUtils() throws Exception {
//		FileExt source = new FileExt("src/main/resources/line-1-10.plain");
//		FileExt dest = new FileExt("line-lt-5.json");
//		
//		dest.deleteIfExists();
//		
//		PlainConnector reader = new PlainConnector(source);
//		PlainConnector writer = new PlainConnector(dest);
//		
//		AwkUtils.runAwkScript("BEGIN{print \"# w1 w2\"} {if ($1 < 5) print $0}", reader, writer);
//		
//		System.out.println("Finished!");
//		LibsLogger.info(AwkUtilsTest.class, "Finished!");
//	}
	
//	@Test
//	public void testAwkUtilsBash() throws Exception {
//		FileExt source = new FileExt("./system.dat");
//		FileExt dest = new FileExt("system-lt-100Myr.json");
//		
//		dest.deleteIfExists();
//		
//		PlainConnector reader = new PlainConnector(source);
//		PlainConnector writer = new PlainConnector(dest);
//		
//		AwkUtils.runAwkScriptBash("BEGIN{print \"# tphys smt\"} {if ($2 < 100) print $2, $3}", reader, writer);
//		
//		System.out.println("Finished!");
//		LibsLogger.info(AwkUtilsTest.class, "Finished!");
//	}
	
	@Test
	public void testAwkUtilsBig() throws Exception {
		FileExt source = new FileExt("./inter-binevol.dat");
		FileExt dest = new FileExt("inter-binevol-BHs.json");
		
		dest.deleteIfExists();
		
		PlainConnector reader = new PlainConnector(source);
		PlainConnector writer = new PlainConnector(dest);
		
		AwkEntry awkEntry = new AwkEntry();
		awkEntry.setScript("BEGIN{print \"# ID1 ID2 IK1 IK2\"} {if ($17 == 14) print $3, $4, $17, $18}");
		
		AwkUtils.runAwkScriptBash(awkEntry, reader, writer);
		
		writer.close();
		for (Row row : writer) {
			System.out.println("Output: " + row);
		}
		
		System.out.println("Finished!");
		LibsLogger.info(AwkUtilsTest.class, "Finished!");
	}
	
//	@Test
//	public void testAwkUtilsBigWithError() throws Exception {
//		FileExt source = new FileExt("./inter-binevol.dat");
//		FileExt dest = new FileExt("inter-binevol-BHs.json");
//		
//		dest.deleteIfExists();
//		
//		PlainConnector reader = new PlainConnector(source);
//		PlainConnector writer = new PlainConnector(dest);
//		
//		AwkUtils.runAwkScriptBash("BEGIN{print \"# ID1 ID2 IK1 IK2\"} {if dd($17 == 14) print $3, $4, $17, $18}", reader, writer);
//		
//		LibsLogger.info(AwkUtilsTest.class, "Finished!");
//	}
}
