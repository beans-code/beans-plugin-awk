package net.beanscode.plugin.awk;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.ReloadPropagator;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.jobs.JobsWorker;

public class AwkEntry extends NotebookEntry {
	
	private static final String META_SCRIPT 		= "META_SCRIPT";
	private static final String META_DS_QUERY 		= "META_DS_QUERY";
	private static final String META_TB_QUERY 		= "META_TB_QUERY";
	private static final String META_OUT_TABLE 		= "META_OUT_TABLE";
//	private static final String META_RUNNING 		= "META_RUNNING";
	private static final String META_OUTPUT_TABLEID = "META_OUTPUT_TABLEID";
	
//	@Expose
//	private String script = null;
	
//	@Expose
//	private String dsQuery = null;
//	
//	@Expose
//	private String tbQuery = null;
	
//	@Expose
//	private String outTable = null;
	
//	@Expose
//	private boolean running = false;
	
//	@Expose
//	private UUID outputTableId = null;
	
	private Dataset datasetCache = null;
	
	private Table outputTableCache = null;
	
	public AwkEntry() {
		
	}
	
	@Override
	public String getSummary() {
		return "AWK script: " + getName();
	}
	
	@Override
	public String getShortDescription() {
		return "AWK script";
	}

	@Override
	public boolean isReloadNeeded() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		start();
		return ReloadPropagator.BLOCK;
	}

	@Override
	public boolean isRunning() throws ValidationException, IOException {
		return getProgress().isRunning();
	}

	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.awk.AwkPanel";
	}
	
	public Connector getInputConnector() {
		return new ConnectorList(TableFactory.iterateTables(getUserId(), getDsQuery(), getTbQuery()));
	}
	
//	public Connector getOutputConnector() {
//		return ((PlainConnector) (new PlainConnector()
//			.setInitParams(new ConnectorInitParams().setTableId(getId()))))
//			.setPath(new File("awk-output-" + getId().getId() + ".dat")); // TODO conf
//		
////		return new MapDbConnector().setInitParams(new ConnectorInitParams().setTableId(getId()));
//	}

	public String getScript() {
		return getMetaAsString(META_SCRIPT, null);
	}

	public AwkEntry setScript(String script) {
		setMeta(META_SCRIPT, script);
		return this;
	}

	public String getDsQuery() {
		return getMetaAsString(META_DS_QUERY, null);
	}

	public AwkEntry setDsQuery(String dsQuery) {
		setMeta(META_DS_QUERY, dsQuery);
		return this;
	}

	public String getTbQuery() {
		return getMetaAsString(META_TB_QUERY, null);
	}

	public AwkEntry setTbQuery(String tbQuery) {
		setMeta(META_TB_QUERY, tbQuery);
		return this;
	}

	public void run(boolean runInBackground) throws ValidationException, IOException {
		getProgress()
			.running(0.0, "Running...")
			.save();
		
		if (runInBackground) {
			new AwkJob(this).save();
		} else {
			runActually();
		}
	}
	
	public Dataset getDataset(boolean createIfNeeded) throws IOException {
		if (datasetCache == null) {
			datasetCache = DatasetFactory.getDataset(getNotebook().getId());
			
			if (datasetCache == null
					&& createIfNeeded) {
				try {
					datasetCache = new Dataset(getUserId(), getNotebook().getName());
					datasetCache.setId(getNotebook().getId());
					datasetCache.setDescription("This is automatically generated Dataset from within a AWK script. It is a read-only Dataset. ");
					datasetCache.save();
					
					datasetCache.makeReadOnly();
				} catch (ValidationException e) {
					throw new IOException("Cannot create Dataset for writing", e);
				}
			}
		}
		
		return datasetCache;
	}
	
	public Table getOutputTable(boolean createIfNeeded) throws ValidationException, IOException {
		if (getDataset(createIfNeeded) == null) {
			if (createIfNeeded)
				LibsLogger.error(AwkEntry.class, "There is no Dataset for AWK " + getId());
			return null;
		}
		
		if (outputTableCache == null) {
			outputTableCache = TableFactory.getTable(getOutputTableId());
			
			if (outputTableCache == null
					&& createIfNeeded) {
				outputTableCache = new Table(getDataset(true), getOutTable());
				outputTableCache.setId(getOutputTableId());
				outputTableCache.getMeta().add("queryId", getId().getId());
	//			table.getConnectorList().addConnector(getAwkEntry().getOutputConnector());
				
				outputTableCache.save();
				
				outputTableCache.makeReadOnly();
			}
		}
		
		return outputTableCache;
	}
	
//	private void saveNotification(NotificationType notificationType, String message) {
//		try {
//			new Notification()
//				.setDescription(message)
//				.setId(getId())
//				.setLocked(true)
//				.setNotificationType(notificationType)
//				.setTitle("AWK script")
//				.setUserId(getUserId())
//				.save();
//		} catch (ValidationException | IOException e) {
//			LibsLogger.error(AwkEntry.class, "Cannot save Notification for AWK " + getId() + ": " + message, e);
//		}
//	}
	
	private void removeOutputTable() throws ValidationException, IOException {
		getOutputTable(true).remove();
		
		// setting new output Table
		setOutputTableId(UUID.random());
		this.outputTableCache = null;
		
		save();
	}
	
	private void runActually() throws ValidationException, IOException {
		LibsLogger.info(AwkEntry.class, "Starting AWK job ", getId());
		
		try {
			// create dataset and table
			Dataset dataset = getDataset(true);
			Table outputTable = getOutputTable(false);
			
			AssertUtils.assertTrue(dataset != null, "Cannot create Dataset for this AWK entry");
			
			// remove previous AWK output Table
			if (outputTable != null)
				removeOutputTable();
			
			outputTable = getOutputTable(true);
			
			AssertUtils.assertTrue(outputTable != null, "Cannot create Table for this AWK entry");
			
			final List<AwkOneInputJob> awkJobs = new ArrayList<AwkOneInputJob>();
			
			// starting jobs, one job per on input table
			for (Table inputTable : TableFactory.iterateTables(getUserId(), getDsQuery(), getTbQuery())) {
				UUID connectorId = UUID.random();
				
				AwkOneInputJob awkJob = new AwkOneInputJob(this, inputTable.getId(), connectorId);
				
				awkJob.save();
				
				awkJobs.add(awkJob);
			}
			
			// waiting until all jobs will be completed
			boolean run = true;
			while (run) {
				run = false;
				
				for (AwkOneInputJob awkJob : awkJobs) {
//					Job awkJobFromDB = JobFactory.getJob(awkJob.getCombinedKey());
//					BeansConnector beansConnector = BeansConnectorFactory.getBeansConnector(getOutputTableId(), connectorId.getId());
					
//					if (beansConnector != null
//							&& (beansConnector.getStatus() == BeansConnectorStatus.COMMITTED
//									|| beansConnector.getStatus() == BeansConnectorStatus.FAILED)) {
//						finished.add(connectorId);
//					}
					
					if (JobsWorker.isDone(awkJob.getChannel(), awkJob.getCreationMs(), awkJob.getId()) == false) {
						run = true;
						break;
					}
				}
				
				if (run == false) {
				
//				if (beansConnectorIds.size() == finished.size()) {
					LibsLogger.info(AwkEntry.class, "Whole AWK script finished. "
//							+ "Input: ", ArrayUtils.toString(beansConnectorIds, ",")
//							+ "Output: ", ArrayUtils.toString(finished, ",")
							)
							;
//					run = false;
				} else {
					try {
						LibsLogger.debug(AwkEntry.class, "AWK " + getId() + " sleeping...");
						Thread.sleep(2000); // TODO conf
					} catch (Throwable e) {
						LibsLogger.error(AwkEntry.class, "Ohh I cannot take a break", e);
					}
				}
			}

			getProgress()
				.ok()
				.save();
		} catch (Exception e) {
			LibsLogger.error(AwkEntry.class, "Cannot run AWK script " + getId(), e);
			getProgress()
				.fail("AWK script " + getName() + " finished with errors: "
						+ (e.getMessage() != null ? e.getMessage() : ""))
				.save();
		} finally {			
			save();
		}
	}

	public String getOutTable() {
		return getMetaAsString(META_OUT_TABLE, null);
	}

	public AwkEntry setOutTable(String outTable) {
		setMeta(META_OUT_TABLE, outTable);
		return this;
	}

	@Override
	public void stop() throws ValidationException, IOException {
		// TODO Auto-generated method stub
	}

	public UUID getOutputTableId() {
		UUID id = getMetaAsUUID(META_OUTPUT_TABLEID, null);
		if (id == null)
			setOutputTableId(id = UUID.random());
		return id;
//		if (this.outputTableId == null)
//			this.outputTableId = UUID.random();
//		return outputTableId;
	}

	public AwkEntry setOutputTableId(UUID outputTableId) {
		setMeta(META_OUTPUT_TABLEID, outputTableId != null ? outputTableId.getId() : null);
//		this.outputTableId = outputTableId;
		return this;
	}

	@Override
	public void start() throws ValidationException, IOException {
		run(true);
	}
}
