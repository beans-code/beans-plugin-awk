package net.beanscode.plugin.awk;

import java.io.IOException;

import org.python.modules.errno;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.BeansConnectorFactory;
import net.beanscode.model.connectors.BeansConnectorStatus;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;

import com.google.gson.annotations.Expose;

public class AwkOneInputJob extends Job {
	
	@Expose
	private UUID awkEntryId = null;
	
	@Expose
	private UUID tableId = null;
	
	@Expose
	private UUID connectorId = null;
	
	private AwkEntry awkEntry = null;
	
	private Table table = null;

	public AwkOneInputJob() {
		super(BeansChannels.CHANNEL_LONG_HIDDEN);
	}
	
	public AwkOneInputJob(AwkEntry awkEntry, UUID tableId, UUID connectorId) {
		super(BeansChannels.CHANNEL_LONG_HIDDEN);
		setAwkEntryId(awkEntry.getId());
		setConnectorId(connectorId);
		setTableId(tableId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {			
			final UUID partId = UUID.random();
			final Connector outputConnector = BeansSettings.getDefaultConnector(getAwkEntry().getOutputTableId(), partId.getId());
			
			AwkUtils.runAwkScriptBash(getAwkEntry(),
					getTable().getConnectorList(), 
					outputConnector);
			
			outputConnector.close();
			
			new BeansConnector(outputConnector, partId.getId())
//				.setTableId(getAwkEntry().getOutputTableId())
				.setStatus(BeansConnectorStatus.COMMITTED)
				.save();
			
//			BeansConnector check = BeansConnectorFactory.getTable(getAwkEntryId(), getConnectorId().getId());
//					
//			LibsLogger.info(AwkOneInputJob.class, "check " + check);
			
			LibsLogger.info(AwkOneInputJob.class, "Finished one file AWK job ", getAwkEntryId());
		} catch (Throwable e) {
			LibsLogger.error(AwkOneInputJob.class, "Could not process with AWK one table " + getTableId(), e);
			
			try {
				new BeansConnector()
					.setId(getConnectorId().getId())
					.setTableId(getAwkEntryId())
					.setStatus(BeansConnectorStatus.FAILED)
					.save();
			} catch (Throwable t) {
				LibsLogger.error(AwkOneInputJob.class, "Cannot save failed BeansConnector", e);
			}
		}
	}

	public UUID getAwkEntryId() {
		return awkEntryId;
	}

	public AwkOneInputJob setAwkEntryId(UUID awkEntryId) {
		this.awkEntryId = awkEntryId;
		return this;
	}
	
	private Table getTable() throws IOException {
		if (table == null) {
			table = TableFactory.getTable(getTableId());
		}
		return table;
	}

	private AwkEntry getAwkEntry() throws IOException {
		if (awkEntry == null)
			awkEntry = (AwkEntry) NotebookEntryFactory.getNotebookEntry(getAwkEntryId());
		return awkEntry;
	}

	private void setAwkEntry(AwkEntry awkEntry) {
		this.awkEntry = awkEntry;
	}

	public UUID getConnectorId() {
		return connectorId;
	}

	public AwkOneInputJob setConnectorId(UUID connectorId) {
		this.connectorId = connectorId;
		return this;
	}

	public UUID getTableId() {
		return tableId;
	}

	public AwkOneInputJob setTableId(UUID tableId) {
		this.tableId = tableId;
		return this;
	}
}
