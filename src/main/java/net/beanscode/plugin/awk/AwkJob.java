package net.beanscode.plugin.awk;

import java.io.IOException;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;

import com.google.gson.annotations.Expose;

public class AwkJob extends Job {
	
	@Expose
	private UUID awkEntryId = null;
	
	private AwkEntry awkEntry = null;

	public AwkJob() {
		super(BeansChannels.CHANNEL_LONG_HIDDEN);
	}
	
	public AwkJob(AwkEntry awkEntry) {
		super(BeansChannels.CHANNEL_LONG_HIDDEN);
		setAwkEntryId(awkEntry.getId());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		getAwkEntry().run(false);
		
		LibsLogger.info(AwkJob.class, "Finished AWK job ", getAwkEntryId());
	}

	public UUID getAwkEntryId() {
		return awkEntryId;
	}

	public AwkJob setAwkEntryId(UUID awkEntryId) {
		this.awkEntryId = awkEntryId;
		return this;
	}

	private AwkEntry getAwkEntry() throws IOException {
		if (awkEntry == null)
			awkEntry = (AwkEntry) NotebookEntryFactory.getNotebookEntry(getAwkEntryId());
		return awkEntry;
	}

	private void setAwkEntry(AwkEntry awkEntry) {
		this.awkEntry = awkEntry;
	}
}
