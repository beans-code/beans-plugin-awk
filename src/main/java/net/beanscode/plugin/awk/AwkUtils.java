package net.beanscode.plugin.awk;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.lang.ProcessBuilder.Redirect;

import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.PluginManager;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.BigFile;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.file.Line;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;

import org.apache.commons.lang.NotImplementedException;
import org.jawk.Main;

public class AwkUtils {
	
	public static String compileScript(final AwkEntry awkEntry, final Connector input) {
		String group = null;
		String awkScriptNew = awkEntry.getScript();
		while ((group = RegexUtils.firstGroup("(\\$\\D[\\w\\d]*)", awkScriptNew)) != null) {
			String awkScriptNewCopy = awkScriptNew;
			
			int index = 1;
			String colName = group.substring(1);
			for (ColumnDef def : input.getColumnDefs()) {
				if (def.getName().equals(colName)) {
					awkScriptNew = awkScriptNew.replace("$" + colName, "$" + index);
				}
				index++;
			}
			
			if (awkScriptNew.equals(awkScriptNewCopy))
				// no changes
				break;
		}
		
		return awkScriptNew;
	}
	
	public static void runAwkScriptBash(final AwkEntry awkEntry, final Connector input, final Connector output) throws Exception {
		// Run command and wait till it's done		
		Watch watch = new Watch();
		
		// changing params names like $tphys to numbers like $5
		String awkScriptNew = compileScript(awkEntry, input);
		LibsLogger.debug(AwkUtils.class, "AWK script after applying column names: ", awkScriptNew);
		
		// working directory in beans folder
		FileExt workingDir = new FileExt(PluginManager.getPluginDefaultFolder(), "/awk/", awkEntry.getId().getId());
		workingDir.mkdirs();
		workingDir.cleanDir();
		
		File awkTmp = new FileExt(workingDir, "awk-" + UUID.random().getId() + ".awk");//FileUtils.createTmpFile("awk-", "");
		awkTmp.deleteOnExit();
		FileUtils.saveToFile(awkScriptNew, awkTmp, true);
		LibsLogger.info(AwkUtils.class, "AWK script saved to ", awkTmp.getAbsolutePath());
		
		ProcessBuilder builder = new ProcessBuilder(new String[]{"/usr/bin/awk", "-f", awkTmp.getAbsolutePath()});

		Process p = builder.start();
		
		final OutputStream stdin = p.getOutputStream();
	    final InputStream stderr = p.getErrorStream();
	    
		
		new Thread() {
			@Override
			public void run() {
		        try {
		        	StringBuilder sb = new StringBuilder();
		        	for (Row row : input.iterateRows(null)) {
						for (net.beanscode.model.plugins.ColumnDef def : input.getColumnDefs()) {
							Object v = row.get(def.getName(), 0.0);
							
							if (StringUtilities.nullOrEmpty(v))
								v = "_";
							
							sb.append(v);
							sb.append(" ");
//							stdin.write(row.get(def.getName(), 0.0).toString().getBytes());
//							stdin.write(" ".getBytes());
						}
//						stdin.write("\n".getBytes());
						sb.append("\n");
						
						if (sb.length() > 500000) {
							stdin.write(sb.toString().getBytes());
							sb.delete(0, sb.length() - 1);
						}
						
//						if (++c == 10)
//							stdin.flush();
					}
		        	
		        	stdin.write(sb.toString().getBytes());
		        	if ((sb.length() - 1) > 0)
		        		sb.delete(0, sb.length() - 1);
		        	
					stdin.flush();
					stdin.close();
				} catch (IOException e) {
					LibsLogger.error(AwkUtils.class, "Exception in AWK script", e);
				}
			}
		}.start();
		

        // Grab output and print to display
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line = "";
        StringBuilder header = new StringBuilder();
        boolean headerDone = false;
        while ((line = reader.readLine()) != null) {
        	if (!headerDone) {
        		if (line.startsWith("#")) {
        			header.append(line);
        			header.append("\n");
        			continue;
        		} 
        		header.append(line); // first row with data
        		header.append("\n");
        		
        		if (header.toString().trim().length() > 0) {
        			UUID fileTmp = UUID.random();
        			FileUtils.saveToFile(header.toString(), new File(fileTmp.getId()), true);
					
        			PlainConnector tmpConn = null;
        			try {
						tmpConn = new PlainConnector(new File(fileTmp.getId()));
						for (Row row : tmpConn.iterateRows(null)) {
							break;
						}
						tmpConn.close();
							
						output.setColumnDefs(tmpConn.getColumnDefs());
        			} finally {
        				if (tmpConn != null)
        					tmpConn.clear();
        			}
        		}
        		
        		headerDone = true;
        	}
        	
        	Row row = new Row();
			Line l = new Line(line);
			for (int i = 0; i < l.size(); i++) {
				row.addColumn(output.getColumnDefs().get(i).getName(), l.get(i));
			}
			if (row.size() > 0)
				output.write(row);
//            System.out.println("out: " + line);
        }
        
        // Grab error message
        BufferedReader readerErr = new BufferedReader(new InputStreamReader(stderr));
        StringBuffer sbErr = new StringBuffer();
        while ((line = readerErr.readLine()) != null) {
        	sbErr.append(line);
        	sbErr.append("\n");
        	LibsLogger.error(AwkUtils.class, "AWK ERROR: " + line);
        }
        
        p.waitFor();
        
		input.close();
		output.close();
		
		if (StringUtilities.notEmpty(sbErr.toString()))
			throw new IOException("AWK script failed: " + sbErr.toString());
		
		LibsLogger.info(AwkUtils.class, "AWK script done in ", watch);
	}

	// TODO something is wrong with this function
	private static void runAwkScript(final String awkScript, final Connector input, final Connector output) throws Exception {
		final PipedInputStream pipeInStr = new PipedInputStream();  
		final PipedOutputStream pipeOutStr = new PipedOutputStream(pipeInStr);
		
//		output.setColumnDefs(input.getInitParams());
//		
//		pout.write("# ".getBytes());
//		for (ColumnDef def : input.getColumnDefs()) {
//			pout.write(def.getName().getBytes());
//			pout.write(" ".getBytes());
//		}
//		pout.write("\n".getBytes());
		
		
		
		
//		FileOutputStream fw = new FileOutputStream("out.dat");
//		PrintStream ps = new PrintStream(fw);
		
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		PrintStream ps = new PrintStream(baos);
//		ByteArrayInputStream in = new ByteArrayInputStream(baos.toByteArray());
		
//		PrintStream err = System.err;// new PrintStream(fw); // TODO save it somehow
		PrintStream err = new PrintStream(new File("awk.error")); // TODO save it somehow
		
		InputStream inTmp = System.in;
		PrintStream outTmp = System.out;
		PrintStream errTmp = System.err;
		
		LibsLogger.info(AwkUtils.class, "test 1");
		
		final UUID fileTmp = UUID.random();
		try {
			
			new Thread() {
				public void run() {
					try {
						for (Row row : input.iterateRows(null)) {
//							for (Map.Entry<String, Object> e : row.getColumns().entrySet()) {
							for (net.beanscode.model.plugins.ColumnDef def : input.getColumnDefs()) {
								pipeOutStr.write(row.get(def.getName(), 0.0).toString().getBytes());
								pipeOutStr.write(" ".getBytes());
							}
							pipeOutStr.write("\n".getBytes());
						}
						pipeOutStr.flush();
						pipeOutStr.close();
						LibsLogger.info(AwkUtils.class, "Input Connector read successfully");
					} catch (IOException e) {
						LibsLogger.error(AwkUtils.class, "AWK script failed", e);
					}
				};
			}.start();
			
			new Main(new String[] {awkScript}, 
					pipeInStr, 
					new PrintStream(new OutputStream() {
//						boolean headerPresent = false;
						boolean readData = false;
						StringBuilder sb = new StringBuilder(5000);
						
						@Override
						public void write(byte[] b, int off, int len) throws IOException {
							try {
								sb.append(new String(b, off, len));
								
	//							LibsLogger.info(AwkUtils.class, "test 2");
	//							System.out.println("dwa");
								
								// if data was already read, header is not expected
								if (readData) {
									if (sb.length() > 10000) {
										int newline = -1;
										while ((newline = sb.indexOf("\n")) >= 0) {
											String lineStr = sb.substring(0, newline);
											if (lineStr.startsWith("#")) {
												sb.delete(0, newline + 1);
												continue;
											}
											
											if (lineStr.length() > 0) {
												Row row = new Row();
												Line line = new Line(lineStr);
												for (int i = 0; i < line.size(); i++) {
													row.addColumn(output.getColumnDefs().get(i).getName(), line.get(i));
												}
												output.write(row);
											}
											
											sb.delete(0, newline + 1);
										}
									}
									return;
								}
								
								// check if there is header and data to read
								FileUtils.saveToFile(sb.toString(), new File(fileTmp.getId()), true);
								int rowsCount = 0;
								for (String line : new BigFile(fileTmp.getId())) {
									if (!line.startsWith("#"))
										rowsCount++;
								}
								
								if (rowsCount > 1) {
									PlainConnector tmpConn = null;
									try {
										tmpConn = new PlainConnector(new File(fileTmp.getId()));
										for (Row row : tmpConn.iterateRows(null))
											if (row.size() > 0)
												rowsCount++;
										tmpConn.close();
										
										output.setColumnDefs(tmpConn.getColumnDefs());
										readData = true;
									} finally {
										if (tmpConn != null)
											tmpConn.clear();
									}
								}
							} catch (Exception e) {
								LibsLogger.error(AwkUtils.class, "AWK script failed", e);
							}
						}
						
						@Override
						public void write(int b) throws IOException {
							throw new NotImplementedException("Do not use method write(int b)");
						}
					}), 
					err);
			
			
			
			
			
		} finally {
			input.close();
			output.close();
			
			new FileExt(fileTmp.getId()).deleteIfExists();
			
			System.in.close();
			System.out.close();
			System.err.close();
			
//			System.in.close();
//			System.out.close();
//			System.err.close();
			System.setIn(inTmp);
			System.setOut(outTmp);
			System.setErr(errTmp);
		}
		
//		PlainConnector plainOnTheFly = new PlainConnector(in);
//		for (Row row : plainOnTheFly.readRows()) {
//			output.write(row);
//		}

//		BigFileSplitted bfs = new BigFileSplitted(in);
//		for (Line line : bfs) {
//			output.write(row);
//		} 
		
		
		
	}
}
