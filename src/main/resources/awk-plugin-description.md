### AWK plugin

AWK plugin allows to read the data using AWK scripts. As an input it takes any number of Tables from any number of Datasets. The output is written into a Table too.

Important links:

* [https://www.gnu.org/software/gawk/manual/gawk.html](AWK manual)